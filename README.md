- Este proyeto pertenece a los integrantes:
	Nibaldo Viveros
	Bastián Zambrano
	Rodrigo Segura

-Los controles del proyecto son:

	- Movimiento del jugador: teclas de izquierda, arriba, abajo, derecha o con el joystick.
	- El objetivo del juego es aguantar los 2 minutos esquivando a los enemigos, obteniendo asi la menor cantidad de toques posibles.
	- Se solicita un nombre de jugar el cual queda guardado en la instancia y se muestra al final del juego.
	- Entre las opciones del menu estan iniciar el juego, salir y las opciones donde se puede ajustar el brillo y el volumen del juego.