﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public TextMeshProUGUI tiempoText;
    public TextMeshProUGUI Vidatxt;
    public TextMeshProUGUI RecordTxt;
    public float tiempo = 60.0f;
    int vida;
    public bool activo;
    Canvas canvas;
    Canvas canvas1;
    public string nombre;
    public float record;
    private string recordPrefabName = "Record";
    public bool gano = false;

    void Start()
    {
        
        Time.timeScale = false ? 0 : 1f;
        canvas = GameObject.Find("Canvas2").GetComponent<Canvas>();
        canvas1 = GameObject.Find("Canvas").GetComponent<Canvas>();
        canvas.enabled = true;
        canvas.enabled = false;
    }

    private void Awake()
    {
        LoadData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetFloat(recordPrefabName, 0.0f);
    }

    private void LoadData()
    {
        record = PlayerPrefs.GetFloat(recordPrefabName);
    }

    void Update()
    {
        vida = GameObject.Find("player").GetComponent<Vida_y_daño>().vida;
        tiempo = tiempo - 1 * Time.deltaTime;
        if (tiempo < 0)
        {
            nombre = GameObject.Find("Controlador Partida").GetComponent<ControladorMenu>().nombre;
            SaveData();
            activo = true;
            canvas1.enabled = false;
            canvas.enabled = true;            
            RecordTxt = GameObject.Find("Record").GetComponent<TextMeshProUGUI>();
            RecordTxt.text = "Nombre: " + nombre + "  \n\nVeces tocado: " + vida;
            Time.timeScale = (activo) ? 0 : 1f;
            
        }
        tiempoText = GameObject.Find("Tiempo").GetComponent<TextMeshProUGUI>();
        tiempoText.text = "Tiempo: " + tiempo.ToString("f0");
        Vidatxt = GameObject.Find("Vidas").GetComponent<TextMeshProUGUI>();
        Vidatxt.text = "Toques: " + vida;
    }


}
