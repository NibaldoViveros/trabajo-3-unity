﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida_y_daño : MonoBehaviour
{
    public int vida;
    public bool invencible = false;
    public float tiempoInve = 1f;
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    public AudioClip Clip;

    private void Start()
    {
        gameObject.AddComponent<AudioSource>();
    }

    public void QuitarVida(int daño)
    {
        if (!invencible && vida > -1)
        {
            vida = vida + daño;
            StartCoroutine(Invulnerabilidad().GetEnumerator());

        }              
    }

    IEnumerable Invulnerabilidad()
    {
        invencible = true;
        source.PlayOneShot(Clip);
        yield return new WaitForSeconds(tiempoInve);
        invencible = false;
    }
}
